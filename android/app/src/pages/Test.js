import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { dispatchTest } from '../redux/actions/'
import { SET_GROUPVALUE } from '../redux/actions'
import styles from '../styles/styles'


class Test extends Component {


    constructor(props) {
        super(props);
    }

    componentDidMount = () => {
        this.findValue()
    }



    findValue = () => {
        let groupValue = []
        let temp = 2
        let result = 3
        for (let i = 0; i < this.props.endValue; i++) {
            result = result + temp
            temp += 2
            groupValue.push(result)
        }

        this.props.dispatch(dispatchTest({ type: SET_GROUPVALUE, payload: { key: "groupValue", value: groupValue } }))
    }

    handlePress = async () => {


       await this.props.dispatch(dispatchTest({ type: SET_GROUPVALUE, payload: { key: "endValue", value: this.props.endValue + 1 } }))
       await this.findValue()
    }


    render() {
            
        return (
            <View style={styles.container}>
                
                    {this.props.groupValue.map( (element,i) => (
                        <Text key={i}> { element } </Text>
                        
                    ))}
                { this.props.endValue != 10 ? 
                <TouchableOpacity
                    style={styles.buttonContainer}
                    onPress={() =>  this.handlePress()}>
                   <Text style={styles.buttonText}>Next Value</Text> 
                </TouchableOpacity> : <Text style={styles.text}>Stop !</Text>
                }
            </View>

        )
    }
}



const mapStateToProps = state => {

    return {
        endValue : state.test.endValue,
        groupValue : state.test.groupValue
    }
}

export default connect(mapStateToProps)(Test)