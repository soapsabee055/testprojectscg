import { combineReducers } from 'redux';
import restaurant from './restaurant'
import test from './test'

const rootReducer = combineReducers({
    restaurant,
    test
    
});

export default rootReducer;

export * from "./restaurant"

export * from "./test"
    