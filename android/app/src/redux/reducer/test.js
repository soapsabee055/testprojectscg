
import { SET_GROUPVALUE } from '../actions'
import { act } from 'react-test-renderer';
const initState = {

    groupValue: [],
    endValue: 5

}

const test = (state = initState, action) => {
  
  
  switch (action.type) {

    case SET_GROUPVALUE:
        const { key,value } = action.payload

        return { ...state, [key]:value }
    default:

      return { ...state }
  }
};

export default test