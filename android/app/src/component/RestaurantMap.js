
import React, { Component } from 'react';
import {

  View,
  Text,


} from 'react-native';

import { FETCH_RESTUARANT_PLACE } from '../redux/actions'
import { connect } from 'react-redux'
import { dispatchRestaurant } from '../redux/actions/restuarant'
import axios from 'axios'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import * as api from '../config/api'

class RestaurantMap extends Component {


  constructor(props) {
    super(props);
  }

  componentDidMount = async () => {

    const restaurant = await axios.get(api.googlePlace)
    this.props.dispatch(dispatchRestaurant({ type: FETCH_RESTUARANT_PLACE, payload: restaurant.data.results }))

  }



  render() {

    return (

      <MapView
        style={{ flex: 1 }}
        provider={PROVIDER_GOOGLE}
        showsUserLocation
        initialRegion={{
          latitude: 13.828253,
          longitude: 100.5284507,
          latitudeDelta: 0.0523962,
          longitudeDelta: 0.0386536
        }}
      >
        {this.props.location.map(element =>(
                
          <Marker 
            key = {element.name}
            coordinate={{ latitude: element.geometry.location.lat, longitude: element.geometry.location.lng }}
            title={element.name}
            description={element.formatted_address}
          >

          </Marker>
        ))
         
        }

      </MapView>

    );
  }
}



const mapStateToProps = state => {

  return {
    location : state.restaurant.location
  }

}

export default connect(mapStateToProps)(RestaurantMap)